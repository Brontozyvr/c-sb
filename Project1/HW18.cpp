﻿#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <string>
#include <cstring>
#include "Helpers.h"

class players
{
public:
	std::string name;
	int points;

	players(std::string _name = "Player", int _points = 0) :
		name(_name), points(_points)
	{
	}

};
void BubbleSort(players* ArrayPlayers, int PlayersNum)
{
	bool swapped = true;
	while (swapped)
	{
		swapped = false;
		for (int i = 0; i < PlayersNum - 1; i++)
		{
			if (ArrayPlayers[i].points < ArrayPlayers[i + 1].points)
			{
				std::swap(ArrayPlayers[i], ArrayPlayers[i + 1]);
				swapped = true;
			}
		}
	}
}



void HW18()
{
	
		using std::cout;

		int n = 5;
		cout << "How many players would you like? ";
		std::cin >> n;

		auto players_arr = new players[n];

		for (int i = 0; i < n; ++i)
		{
			cout << "\n" << "Enter player " << i + 1 << " name: ";
			std::cin >> players_arr[i].name;
			cout << "Enter player " << i + 1 << " points: ";
			std::cin >> players_arr[i].points;
		}

		cout << "\nList before sorting:" << "\n";

		for (int i = 0; i < n; i++)
		{
			cout << "Player " << players_arr[i].name << " have "
				<< players_arr[i].points << " points" << '\n';
		}

		cout << "\nNow sorted by points:" << "\n";
		BubbleSort(players_arr, n);

		for (int i = 0; i < n; i++)
		{
			cout << "Player " << players_arr[i].name << " have "
				<< players_arr[i].points << " points" << '\n';
		}
		delete[] players_arr;
		cout << "\n";
}


