#include <iostream>
#include <stdio.h>
#include <string>
#include <cstring>
#include <sstream>
#include "Helpers.h"




class Animal
{
public:
    virtual std::string Voice()
    {
        return "Animal gose WOW\n";
    }
};
class Dog : public Animal
{
public:
    std::string Voice() override
    {
        return "Dog gose woof\n";
    }
};
class Cat : public Animal
{
public:
    std::string Voice() override
    {
        return "Cat gose meow\n";
    }
};
class Bird : public Animal
{
public:
    std::string Voice() override
    {
        return "Bird gose tweet\n";
    }
};
class Mouse : public Animal
{
public:
    std::string Voice() override
    {
        return "Mouse gose squeek\n";
    }
};
void HW19()
{




    Dog Dog;
    std::string D = Dog.Voice();
    void* DogVoice = &D;

    Cat Cat;
    std::string C = Cat.Voice();
    void* CatVoice = &C;

    Bird Bird;
    std::string B = Bird.Voice();
    void* BirdVoice = &B;

    Mouse Mouse;
    std::string M = Mouse.Voice();
    void* MouseVoice = &M;


    std::cout << "What dose an animal say?\n \n";


    std::string Sounds[4]{ *(static_cast<std::string*>(DogVoice)), *(static_cast<std::string*>(CatVoice)), *(static_cast<std::string*>(BirdVoice)), *(static_cast<std::string*>(MouseVoice)) };


    for (int i = 0; i < 4; i++)
    {
        std::cout << Sounds[i];
    }



    std::cout << "\n";
}
